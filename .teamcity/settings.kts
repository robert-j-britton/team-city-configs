package _Self.buildTypes
import jetbrains.buildServer.configs.kotlin.v10.vcs.GitVcsRoot
import jetbrains.buildServer.configs.kotlin.v2018_2.*
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.dotnetBuild
import jetbrains.buildServer.configs.kotlin.v2018_2.buildSteps.dotnetClean

version = "2019.1"

project {
    vcsRoot{
        LibraryVcs
    }
    buildType(Build)
}

object Build : BuildType({
    name = "Build"

    vcs {
        LibraryVcs
    }

    artifactRules = "*"

    steps {
        dotnetClean { }
        dotnetBuild { }
    }
})

object LibraryVcs : GitVcsRoot({
    name = "LibraryVcs"
    url = "http://localhost:3000/bb"
    branchSpec = """
        +:refs/heads/(master)
        +:refs/heads/(feature*)
    """.trimIndent()
})
